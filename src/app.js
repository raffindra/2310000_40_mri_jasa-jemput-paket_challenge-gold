document.addEventListener('alpine:init', () => {
  Alpine.data('products', () => ({
    items: [
      { id: 1, name: 'Azura', img: '1.jpg', price: '135000' },
      { id: 2, name: 'Alena', img: '2.jpg', price: '135000' },
      { id: 3, name: 'Eshal', img: '3.jpg', price: '135000' },
      { id: 4, name: 'Carissa', img: '4.jpg', price: '135000' },
      { id: 5, name: 'Hafeeza', img: '5.jpg', price: '135000' },
    ],
  }))

  Alpine.store('cart', {
    items: [],
    total: 0,
    quantity: 0,
    add(newItem) {
      //cek barang yang sama di cart
      const cartItem = this.items.find((item) => item.id === newItem.id)
      // jika belum ada
      if (!cartItem) {
        this.items.push({ ...newItem, quantity: 1, total: newItem.price })
        this.quantity++
        this.total += newItem.price
      } else {
        //jika barang ada di cart, cek apakah barang sama atau beda yang ada di cart
        this.items = this.items.map((item) => {
          //jika barang berbeda
          if (item.id !== newItem.id) {
            return item
          } else {
            //jika barang sudah ada, tambah quantity dan total harga
            item.quantity++
            item.total = item.price * item.quantity
            this.quantity++
            this.total += item.price
            return item
          }
        })
      }

      console.log(this.total)
    },
    remove(id) {
      //ambil item yang mau diremove
      const cartItem = this.items.find((item) => item.id === id)

      //jika barang lebih dari 1
      if (cartItem.quantity > 1) {
        //telusuri 1 1
        this.items = this.items.map((item) => {
          //jika bukan barang yang diklik
          if (item.id !== id) {
            return item
          } else {
            item.quantity--
            item.total = item.price * item.quantity
            item.quantity--
            this.total -= item.price
            return item
          }
        })
      } else if (cartItem.quantity === 1) {
        //jika barangnya sisa 1
        this.items = this.items.filter((item) => item.id !== id)
        this.quantity--
        this.total -= cartItem.price
      }
    },
  })
})

// Konversi ke Rupiah
const rupiah = (number) => {
  return new Intl.NumberFormat('id-ID', {
    style: 'currency',
    currency: 'IDR',
    minimumFractionDigits: 0,
  }).format(number)
}
